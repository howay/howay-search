# howaySearch 说明文档
## 项目用途
    全文搜索引擎

## 使用方法
    


## 技术准备
    后端服务拟采用java，其中jdk版本基于17进行开发，使用springboot框架，数据库使用mybatis，使用dubbo进行分布式开发。

## 项目框架
    howaySearch-core: 数据库核心层
    howaySearch-processor： 处理器层，各服务核心处理逻辑
    howaySearch-util: 工具库
    howaySearch-api: dubbo api
    howaySearch-provider: api服务提供
    howaySearch-web: restful api提供


