package cn.ihoway;

import cn.ihoway.container.HowayContainer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

/**
 * 初始化dubbo服务
 */
@SpringBootApplication
public class ServiceInit {
    public static void main(String[] args) throws IOException {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("dubbo/provider.xml");
        //System.out.println(context.getDisplayName() + ": here");
        context.start();
        System.out.println("*** howaySearch服务已经启动 ***");
        System.in.read();
        HowayContainer container = new HowayContainer();
        container.start();
        SpringApplication.run(ServiceInit.class, args);
    }
}
