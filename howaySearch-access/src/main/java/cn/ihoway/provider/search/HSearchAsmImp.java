package cn.ihoway.provider.search;

import cn.ihoway.api.HSearchAsm;
import cn.ihoway.seg.DocSegProcessor;
import cn.ihoway.seg.io.DocSegInput;
import cn.ihoway.seg.io.DocSegOutput;
import cn.ihoway.util.HowayLog;

import java.util.List;
import java.util.Map;

public class HSearchAsmImp implements HSearchAsm {

    private final HowayLog logger = new HowayLog(HSearchAsmImp.class);

    @Override
    public void createIndex(List<Map<String, String>> docList, String appName, String eventNo, String traceId) {
        DocSegProcessor processor = new DocSegProcessor();
        DocSegInput input = new DocSegInput();
        DocSegOutput output = new DocSegOutput();
        input.inChomm.docList = docList;
        input.inChomm.appName = appName;
        input.eventNo = eventNo;
        input.traceId = traceId;
        processor.doExecute(input,output);
    }

    /**
     * todo 查询，先直接使用缓存存储相关字段
     * @param keySentence
     * @param appName
     * @param eventNo
     * @param traceId
     * @return
     */
    @Override
    public List<Map<String, String>> search(String keySentence, String appName, String eventNo, String traceId) {
        return null;
    }
}
