package cn.ihoway.api;

import java.util.List;
import java.util.Map;

/**
 *
 */
public interface HSearchAsm {
    //创建索引
    public void createIndex(List<Map<String,String>> docList,String appName,String eventNo,String traceId);
    //查询文档
    public List<Map<String,String>> search(String keySentence,String appName,String eventNo,String traceId);
}
