package cn.ihoway.entity;

import cn.ihoway.util.HowayCoreConfigReader;
import cn.ihoway.util.HowayIO;

import java.util.HashSet;
import java.util.Set;

/**
 * 内置词典
 * todo 用户自定义词典、支持用户补充词典，支持多种模式，单个词典模式（用户词典/内置词典），多词典模式
 */
public class Dict {
    public static final Set<String> dict = new HashSet<String>();

    public static void add(String word) {
        dict.add(word);
    }
    public static void remove(String word) {
        dict.remove(word);
    }
    public static int size() {
        return dict.size();
    }
    public static boolean isEmpty() {
        return dict.isEmpty();
    }
    public static boolean contain(String str) {
        //str = str.toLowerCase();
        return dict.contains(str);
    }

    /**
     * 词典初始化
     */
    public static void init() {
        String allPath = HowayCoreConfigReader.getConfig("howay.properties","dict.path");
        String[] pathList = allPath.split(";");
        for(String path : pathList){
            HowayIO.readFile(path, dict);
        }

    }
}
