package cn.ihoway.entity;

import java.util.Objects;

/**
 * 文档是否一致只有标题和内容有关
 */
public class DocInfo {
    private String id; // 文档id
    private String url; //文档url
    private String title; //标题
    private String content; //内容

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DocInfo docInfo = (DocInfo) o;
        return Objects.equals(title, docInfo.title) && Objects.equals(content, docInfo.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, content);
    }

}
