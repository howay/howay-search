package cn.ihoway.entity;

import cn.ihoway.util.HowayCoreConfigReader;
import cn.ihoway.util.HowayIO;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Objects;

/**
 * 索引
 * 默认索引结构：[id url tf lenth pos title]
 */
public class Index {
    private String id; //文档id
    private String url; //文档的具体网址
    private BigDecimal tf; //关键词在该文档中出现的频率（词语出现次数/该文档全部词语数量）
    private int length; //该文档的长度
    private ArrayList<String> pos; //词语在文档中的具体位置，它的结构为：[55:56, 90:91, 102:103, 139:140, 190:191]，起始位置：终止位置，每一对用逗号分隔开
    private String title; //该文档的标题
    private String name; //word_name

    /**
     * 获取索引结构
     */
    public String getIndex(){
        return id + " " + url + " " + tf + " " + length + " " + pos.toString() + " " + title;
    }

    /**
     * 索引创建
     * 索引结构：[id url tf lenth pos title]
     * dir 各项目目录（支持多层结构）
     */
    public void create(String dir){
        String newDir = HowayCoreConfigReader.getConfig("howay.properties","index.path") + "/" + dir + "/";
        String content = getIndex() + "\r\n";
        HowayIO.fileWriter(newDir,name + ".ind",content,true);
    }

    /**
     * 索引创建
     * dir 各项目目录（支持多层结构）
     * 自定义索引结构，使用自定义索引结构，则查询时也需要使用自定义索引结构
     */
    public void create(String dir,String indexStruct){
        String newDir = HowayCoreConfigReader.getConfig("howay.properties","index.path") + "/" + dir + "/";
        String content = indexStruct + "\r\n";
        HowayIO.fileWriter(newDir,name + ".ind",content,true);
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public BigDecimal getTf() {
        return tf;
    }

    public void setTf(BigDecimal tf) {
        this.tf = tf;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public ArrayList<String> getPos() {
        return pos;
    }

    public void setPos(ArrayList<String> pos) {
        this.pos = pos;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Index index = (Index) o;
        return length == index.length && Objects.equals(id, index.id) && Objects.equals(url, index.url) && Objects.equals(tf, index.tf) && Objects.equals(pos, index.pos) && Objects.equals(title, index.title) && Objects.equals(name, index.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, url, tf, length, pos, title, name);
    }
}
