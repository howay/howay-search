package cn.ihoway.seg;

import cn.ihoway.entity.Index;
import cn.ihoway.util.HowayLog;

import java.util.List;

public class DocSegThread extends Thread {
    private Integer threads = 0;
    private List<Index> indexList;
    private String dir;
    private static final Object lock = new Object();
    private final HowayLog logger = new HowayLog(DocSegThread.class);

    public void setDir(String dir) {
        this.dir = dir;
    }

    public void setThreads(int threads) {
        this.threads = threads;
    }

    public void setIndexList(List<Index> indexList) {
        this.indexList = indexList;
    }

    private void print() throws InterruptedException {
        while (true){
            synchronized (this){
                if(indexList.size() > 0){
                    synchronized (lock){
                        Index index = indexList.get(0);
                        if(!"$cnt".equals(index.getName())){
                            //logger.info("线程" + Thread.currentThread().getName() + " work with " + index.getName());
                            index.create(dir);
                        }
                        indexList.remove(0);
                    }
                }else{
                    synchronized (threads){
                        threads--;
                        //logger.info("线程" + Thread.currentThread().getName() + " over");
                        if(threads == 0){
                            logger.info("所有线程都已结束任务！");
                        }
                    }
                    break;
                }
            }
        }
    }

    @Override
    public void run(){
        try {
            print();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }



}
