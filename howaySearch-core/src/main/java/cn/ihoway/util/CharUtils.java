package cn.ihoway.util;

public class CharUtils {
    //字符全角转半角
    public static char ToDBC(char ch) {
        if (ch == 12288) {
            return 32;
        }
        else if (ch > 65280 && ch < 65375) {
            return (char) (ch - 65248);
        }
        else if (ch >= 'A' && ch <= 'Z') {
            return (ch += 32);
        }
        return ch;
    }
}
