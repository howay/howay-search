package cn.ihoway.util;

public class hStringUtils {

    /**
     * 全角转半角
     * @param input str
     * @return str
     */
    public static String ToDBC(String input) {
        char[] ch = input.toCharArray();
        for (int i = 0; i < ch.length; i++) {
            ch[i] = CharUtils.ToDBC(ch[i]);
        }
        return new String(ch);
    }

}
