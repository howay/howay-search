package cn.ihoway.analysis;

import cn.ihoway.util.hStringUtils;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FilterTest {
    Filter filter = new Filter();
    @Test
    void filter() {
        String str = "你好吗罢了abc BN789，@#￥%……&*|\\/;'?（）！~！@#()@#$%&*@!#$%！＠＃＄％＾＠＄％＆＊！＠＃＄Ｍｎｄｊｓｅ１２３４234234fg";
        str = hStringUtils.ToDBC(str);
        //str = str.replaceAll("[\\pP‘’“”]", "");
        str = Filter.filter(str);
        System.out.println(str);

    }

    @Test
    void stringTest(){
        String str = "123a";
        boolean isNum = str.matches("^[0-9]+$");
        boolean isWord = str.matches("^[a-zA-Z]+$");
        System.out.println(isNum+" "+isWord);
    }

     public int search(int[] nums, int target) {
        int begin = 0;
        int end = nums.length;
        int pos = (begin + end) / 2;
        int current = nums[pos];
        while(current != target){
            if(pos == begin || pos == end){
                if(target < current){
                    return pos;
                }else {
                    return pos + 1;
                }

            }
            if(current > target){
                end = pos;
            }else {
                begin = pos;
            }
            pos = (begin + end) / 2;
            current = nums[pos];

        }
        return pos;
    }

    int isBad(int n,int isBad){
        int mid = n / 20;
        if(mid >= isBad){
            return isBad(mid,isBad);
        }else {
            return isBad(mid,n,isBad);
        }
    }

    int isBad(int begin,int end,int isBad){
        if( isBad == end && begin < isBad){
            return end;
        }
        int mid =  begin + (end - begin) / 2;
        if(mid >= isBad){
            end = mid;
        }else {
            begin = mid;
        }

        return isBad(begin,end,isBad);
    }

    public int[] sortedSquares(int[] nums) {
        int left = 0;
        int right = nums.length - 1;
        int[] res = new int[nums.length];
        int pos = nums.length - 1;
        while (left <= right){
            if( nums[left] * nums[left] <= nums[right] * nums[right]){
                res[pos--] = nums[right] * nums[right];
                right--;
            }else {
                res[pos--] = nums[left] * nums[left];
                left++;
            }
        }
        res[right] = nums[right] * nums[right];
        return res;
    }

    public void reverse(int[] nums,int begin,int end){
        while (begin<=end){
            int temp = nums[begin];
            nums[begin] = nums[end];
            nums[end] = temp;
            begin++;
            end--;
        }
    }


    public void rotate(int[] nums, int k) {
        k %= nums.length;
        reverse(nums,0,nums.length - 1);
        reverse(nums,0,k - 1);
        reverse(nums,k,nums.length - 1);

    }

    /*
    *   0,1,0,3,12
    *   1,0,3,12,0
    *
     */
    public void moveZeroes(int[] nums) {
        int cnt = 0;

        for(int i=0;(i+cnt)<nums.length;i++){
            int total = 0;
            if(nums[i] == 0){
                for(int j=i+1;j<nums.length;j++){
                    if(nums[j]!=0){
                        break;
                    }
                    cnt++;
                    total++;
                }
                reverse(nums,i,nums.length - 1 - cnt + total);
                reverse(nums,i,nums.length - 2 - cnt);
                cnt++;
            }
        }
        for (int i=0;i<nums.length;i++){
            System.out.println(nums[i]);
        }
    }

    public int[] twoSum(int[] numbers, int target) {
        int left = 0;
        int right = numbers.length - 1;
        while (left < right){
            if ((numbers[left] + numbers[right]) > target){
                right--;
            }else if((numbers[left] + numbers[right]) < target){
                left++;
            }else {
                break;
            }
        }
        int[] res = new int[2];
        res[0] = left+1;
        res[1] = right+1;
        System.out.println(left+":"+right);
        return res;
    }

    public String reverseString(char[] s) {
        int left = 0;
        int right = s.length - 1;
        while (left < right){
            char temp = s[left];
            s[left] = s[right];
            s[right] = temp;
            left++;
            right--;
        }
        return String.valueOf(s);
    }

    public String reverseWords(String s) {
        String[] strArray = s.split(" ");
        s = "";
        for(int i = 0;i < strArray.length;i++){
            s += reverseString(strArray[i].toCharArray());
            if(i != (strArray.length - 1)){
                s += " ";
            }
        }
        return s;
    }

     public class ListNode {
        int val;
        ListNode next;
        ListNode() {}
        ListNode(int val) { this.val = val; }
        ListNode(int val, ListNode next) { this.val = val; this.next = next; }
     }

     //12345678910
    //12345
    public ListNode middleNode(ListNode head) {
        ListNode low = head;
        ListNode quick = head.next;
        if(quick == null){
            return low;
        }
        while (quick.next != null){
            quick = quick.next;
            low = low.next;
            if(quick.next == null){
                System.out.println("xx");
                System.out.println(low.val);
                System.out.println(quick.val);
                return low;
            }
            quick = quick.next;
        }
        return low.next;
    }

    //123456789  3
    //1234
    public ListNode removeNthFromEnd(ListNode head, int n) {
        ListNode zoroHead = new ListNode(0, head);
        ListNode slow = head;
        ListNode fast = zoroHead;
        while(n != 0){
            fast = fast.next;
            n--;
        }
        while (fast!=null){
            slow = slow.next;
            fast = fast.next;
        }
        slow.next = slow.next.next;
        ListNode res = zoroHead.next;
        return res;
    }

    @Test
    void solutionTest(){
        ListNode node = new ListNode();
        node.val = 1;
        ListNode first = node;
        for(int i=2;i<4;i++){
            ListNode second = new ListNode();
            second.val = i;
            first.next = second;
            first = second;
        }
//        while (node != null){
//            System.out.println(node.val);
//            if(node.next == null){
//                break;
//            }
//            node = node.next;
//        }
        ListNode res = removeNthFromEnd(node,2);
        while (res!=null){
            System.out.println(res.val);
            if(res.next==null){
                break;
            }
            res = res.next;
        }
    }


}