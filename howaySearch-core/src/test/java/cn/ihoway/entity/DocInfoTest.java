package cn.ihoway.entity;

import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class DocInfoTest {

    @Test
    void testEquals() {
        DocInfo doc1 = new DocInfo();
        doc1.setContent("2");
        doc1.setId("1");
        doc1.setTitle("123");
        DocInfo doc2 = new DocInfo();
        doc2.setContent("2");
        doc2.setId("2");
        doc2.setTitle("123");
        System.out.println(doc1.equals(doc2));
        Set<DocInfo> set = new HashSet<>();
        set.add(doc1);
        set.add(doc2);
        System.out.println(set.size());
    }
}