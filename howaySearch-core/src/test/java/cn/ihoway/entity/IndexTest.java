package cn.ihoway.entity;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class IndexTest {

    private Index index = new Index();

    void before(){
        index.setId("1");
        index.setUrl("/essay/1");
        index.setLength(100);
        index.setTf(new BigDecimal("1.2"));
        index.setName("测试");
        ArrayList<String> pos = new ArrayList<>();
        pos.add("55:56");
        pos.add("90:91");
        index.setPos(pos);
        index.setTitle("仅仅是测试");
    }

    @Test
    void create() {
        before();
        index.create("测试");
    }

    @Test
    void testCreate() {
        before();
        index.create("测试/测试2","/essay/1 100 1.2 [55:56, 90:91, 102:103] 仅仅是测试");
    }
}