package cn.ihoway.seg;

import cn.ihoway.common.CommonProcessor;
import cn.ihoway.entity.DocInfo;
import cn.ihoway.entity.Index;
import cn.ihoway.seg.io.DocSegInput;
import cn.ihoway.seg.io.DocSegOutput;
import cn.ihoway.type.StatusCode;
import cn.ihoway.util.HowayLog;
import cn.ihoway.util.HowayResult;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Map;

/**
 *todo 用api的形式对外提供服务，各调用方自定义查询配置进行分割和查询
 * 查询配置自定义包括：
 *    1、词典模式，单词典模式（用户词典/内置词典）和多词典模式
 *    2、查询/分割文档范围 支持提供文档内容/文档本机地址目录，支持同时提供N个文档，数量不限制（第一种模式可能需要限制）
 *    3、索引分区名称
 *
 *
 */
public class DocSegProcessor  extends CommonProcessor<DocSegInput, DocSegOutput> {
    private final HowayLog logger = new HowayLog(DocSegProcessor.class);

    @Override
    protected StatusCode dataCheck(DocSegInput input){
        if(StringUtils.isBlank(input.inChomm.appName)){
            logger.info("DocSegProcessor dataCheck error:appName为空！");
            return StatusCode.FIELDMISSING;
        }
        if(input.inChomm.docList == null || input.inChomm.docList.size() == 0){
            logger.info("DocSegProcessor dataCheck error:docList为空！");
            return StatusCode.FIELDMISSING;
        }
        return StatusCode.SUCCESS;
    }
    
    @Override
    protected HowayResult process(DocSegInput input, DocSegOutput output) {
        DocSeg docSeg = new DocSeg();
        //从数据库中读取该应用appName的全部文档数量以及文档平均长度
        ArrayList<DocInfo> docList = new ArrayList<>();
        for(Map<String,String> doc:input.inChomm.docList){
            DocInfo docInfo = new DocInfo();
            docInfo.setContent(doc.get("content"));
            docInfo.setUrl(doc.get("url"));
            docInfo.setTitle(doc.get("title"));
            docInfo.setId(doc.get("id"));
            docList.add(docInfo);
            //文档数量加1
        }
        //更新文档数量和平均长度
        ArrayList<Index> indexList = docSeg.multipleDocSeg(docList);
        docSeg.createIndex(indexList,input.inChomm.appName);
        return HowayResult.createSuccessResult(output);
    }
}