package cn.ihoway.seg.io;

import cn.ihoway.annotation.InputCheck;
import cn.ihoway.annotation.InsideCheck;
import cn.ihoway.common.CommonSeria;
import cn.ihoway.common.io.CommonInput;

import java.util.List;
import java.util.Map;

@InputCheck(check = true)
/**
 * DocSegInput类是CommonInput的子类，用于处理文档分段的输入
 * 该类的目的是专门针对文档分段任务，继承自CommonInput类，预设了通用的输入属性
 */
public class DocSegInput extends CommonInput {

    public InChomm inChomm = new InChomm();

    @InputCheck(check = true)
    public static class InChomm extends CommonSeria {
        @InsideCheck(mustInput=true)
        public List<Map<String,String>> docList; //直接送文档内容，包括文档id、文档url、标题和内容
        @InsideCheck(mustInput=true)
        public String appName; //应用名称 --索引目录

    }
}
